import React from 'react';
import './App.css';
import Table from './components/Table';
import Filter from './components/Filter';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {filter: "All"};
    }

    componentDidMount() {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "http://localhost:8000/analysis/");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                const state = this.expandState(response, this.state.filter)
                this.setState({tableData: state});
            };
        };
        const request =  {
            strategy_name: 'SMAO', 
            short_field: 'close', 
            short_window: 20, 
            long_field: 'close', 
            long_window: 10
        }
        xhr.send(JSON.stringify(request));
    };

    expandState = (response, filter) => {
        var data = response.data;
        var expanded = data.map((row) => (
            {data: row, hidden: this.getHiddenStatus(filter, row)}
        ));
        response.data = expanded
        return (response);
  };

    getHiddenStatus = (filter, row) => {
        if (filter === "All") {
            return (false);
        } else if (row.includes(filter)) {
            return (false);
        } else {
            return (true);
        }
    };

    updateFilter = (event) => {
        let filter = event.target.value;
        var tableData = {...this.state.tableData};
        var data = tableData.data;
        data.forEach(
            element => element.hidden = this.getHiddenStatus(filter, element.data)
        );
        this.setState({filter: filter, tableData: tableData});
    };

    render() {
        if (typeof this.state.tableData != "undefined") {
            return (
                <div className="App">
                    <Filter updateFilter={this.updateFilter} />
                    <Table data={this.state.tableData} />
                </div>
            );
        } else {
            return (<div className="App" />);
        }
    };
}

export default App;
