import React from 'react';

function Table(props) {
    const visible = props.data.data.filter(data => !data.hidden);
    var i = 0;
    const rows = visible.map((data) => (
        <TableRow key={i++} className="tableRow" data={data.data} />
    ));
    return(
        <div id="Table">
            <table>
                <tbody>
                    <TableRow 
                        className="tableHeader" 
                        data={props.data.columns} 
                        header={true} 
                    />
                    {rows}
                </tbody>
            </table>
        </div>
    );
}

function TableRow(props) {
    var i = 0;
    const row = props.data.map((datum) => (
        <TableCell key={i++} datum={datum} header={props.header} />
    ));
    return(
        <tr className={props.className}>{row}</tr>
    );
}

function TableCell(props) {
    const datum = props.datum
    if (props.header) {
        return(
            <th align="left">{datum}</th>
        );
    } else {
        return(
            <td style={inlineCellStyle(datum)}>{datum}</td>
        );
    };
}

function inlineCellStyle(datum) {
    if (datum === "BUY") {
        return({backgroundColor: "lightgreen"});
    } else if (datum === "SELL") {
        return({backgroundColor: "lightcoral"});
    } else if (datum === "WAIT TO BUY" || datum === "WAIT TO SELL") {
        return({backgroundColor: "lightgray"});
    } else {
        return({});
    }
};

export default Table;