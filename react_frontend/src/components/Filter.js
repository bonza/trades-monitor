import React from 'react';

function Filter(props) {
    return(
        <div id="Filter">
            <label>Filter by action:</label>
            <select id="filter" onChange={props.updateFilter}>
                <option value="All">All</option>
                <option value="BUY">BUY</option>
                <option value="SELL">SELL</option>
                <option value="WAIT TO BUY">WAIT TO BUY</option>
                <option value="WAIT TO SELL">WAIT TO SELL</option>
            </select>
        </div>
    );
};

export default Filter;