import json
import numpy as np
import pandas as pd
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from tsbp import strategies, screen
from tsbp.config import DB_PATH
from sdmi.load import Loader


COMPANIES_PATH = "/Users/Ben/Desktop/hgc/company_lists/20200101-all-ords.csv"

SCREEN_DICT = {
    'RSI': {'strategy': strategies.RSI, 'screener': screen.RSIScreener},
    'SMAO': {'strategy': strategies.SMAO, 'screener': screen.SMAOScreener},
}


def _make_table_data(strategy_name, params):
    """
    

    Parameters
    ----------
    strategy_name : TYPE
        DESCRIPTION.
    params : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    # get current actions based on provided strategy
    screener = SCREEN_DICT[strategy_name]['screener'](DB_PATH, None, **params)
    data = screener.get_screener_data()
    # calculate difference between current and target prices
    data['Difference (%)'] = np.round(
        -100 * (data['Current'] - data['Target']) / data['Current'], 2
    )
    data = data.round(2)
    data = data.applymap(str)
    data.index.name = 'Code'
    data_cols = []
    for i in list(data.columns):
        if i != 'Current':
            data_cols.append(i)
        else:
            data_cols.append('Prev. Close')
    data.columns = data_cols
    # read in company information, to be included alongside the numerical data
    companies = pd.read_csv(COMPANIES_PATH, skiprows=0, header=1)
    company_cols = ['Code', 'Company', 'Sector']
    companies = companies[company_cols].set_index('Code')
    data = data.join(companies)
    data.reset_index(inplace=True)
    # reorder dataframe
    data = data[company_cols + data_cols]
    # make 'Action' last column and capitalise
    action = data['Action']
    del data['Action']
    data['Action'] = action.str.upper()
    # convert dataframe to json response and send
    data = json.loads(data.to_json(orient='split'))
    # 'index' isn't used by the front end
    del data['index']
    return data


def _make_stats_data(stocks, stocks_data, strategy_name, params):
    """
    

    Parameters
    ----------
    strategy_name : TYPE
        DESCRIPTION.
    params : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    strategy = SCREEN_DICT[strategy_name]['strategy'](stocks_data, **params)
    #
    date = stocks_data.index[-1]
    dates = [date.strftime('%Y-%m-%d')]
    #
    for i in range(2):
        date -= pd.Timedelta('1Y')
        dates.append(date.strftime('%Y-%m-%d'))
    #
    dates = list(reversed(dates))
    results = []
    for i in range(len(dates) - 1):
        result = strategy.aggregate_backtest(
            stocks, start=dates[i], end=dates[i+1], fee=0.1, compare=True
        )
        results.append(result)
    results = pd.concat(results, axis=0)
    results.dropna(axis=0, how='any', inplace=True)
    means = np.round(results.mean(), 2)
    stds = np.round(results.std(), 2)
    data = {
        'strategy': {
            'mean': means['strategy_return'], 'std': stds['strategy_return']
        }, 
        'market': {
            'mean': means['market_return'], 'std': stds['market_return']
        },
    }
    return data
    

@csrf_exempt
def table(request):
    """
    

    Parameters
    ----------
    request : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    params = json.loads(request.body)
    strategy_name = params['strategy_name'] 
    del params['strategy_name']
    data = _make_table_data(strategy_name, params)
    data = JsonResponse(data, safe=False)
    return data
